﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour {

    private int entrance;
    public Transform player;
    public Transform target;
    public bool tp = false;

    private void Update()
    {
        Vector3 targetDir = target.position - player.position;
        Vector3 newDir = Vector3.RotateTowards(player.forward, targetDir, 100.0f, 0.0f);
        player.rotation = Quaternion.LookRotation(newDir);

    }

    private void OnTriggerEnter(Collider other)
    {
        tp = true;
        entrance = Random.Range(1, 5);
        if (entrance == 1)
        {
            //GameObject target = other.GetComponent<GameObject>();

            other.transform.position = new Vector3(10, 1, 5);
            Vector3 targetDir = target.position - player.position;
            Vector3 newDir = Vector3.RotateTowards(player.forward, targetDir, 100.0f, 0.0f);
            player.rotation = Quaternion.LookRotation(newDir);

            //player.transform.rotation = new Quaternion(0.0f, -135.0f, 0.0f, other.transform.rotation.w);
            //other.transform.localEulerAngles = new Vector3(0.0f, -135.0f, 0.0f);
            //other.transform.rotation.Set(0.0f, -135.0f, 0.0f, other.transform.rotation.w);

        }
        if (entrance == 2)
        {
            //GameObject target = other.GetComponent<GameObject>();

            other.transform.position = new Vector3(10, 1, -3);
            Vector3 targetDir = target.position - player.position;
            Vector3 newDir = Vector3.RotateTowards(player.forward, targetDir, 100.0f, 0.0f);
            player.rotation = Quaternion.LookRotation(newDir);
            //player.transform.rotation = new Quaternion(0.0f, -45.0f, 0.0f, other.transform.rotation.w);
            //other.transform.localEulerAngles = new Vector3(0.0f, -45.0f, 0.0f);
            //other.transform.rotation.Set(0.0f, -45.0f, 0.0f, other.transform.rotation.w);
        }

        if (entrance == 3)
        {
            //GameObject target = other.GetComponent<GameObject>();

            other.transform.position = new Vector3(-5.7f, 1, 5.7f);
            Vector3 targetDir = target.position - player.position;
            Vector3 newDir = Vector3.RotateTowards(player.forward, targetDir, 100.0f, 0.0f);
            player.rotation = Quaternion.LookRotation(newDir);
            //player.transform.rotation = new Quaternion(0.0f, 135.0f, 0.0f, other.transform.rotation.w);
            //other.transform.localEulerAngles = new Vector3(0.0f, 135.0f, 0.0f);
            //other.transform.rotation.Set(0.0f, 135.0f, 0.0f, other.transform.rotation.w);
        }

        if (entrance == 4)
        {
            //GameObject target = other.GetComponent<GameObject>();

            other.transform.position = new Vector3(-5.7f, 1, -2.7f);
            Vector3 targetDir = target.position - player.position;
            Vector3 newDir = Vector3.RotateTowards(player.forward, targetDir, 100.0f, 0.0f);
            player.rotation = Quaternion.LookRotation(newDir);
            //player.transform.rotation = new Quaternion(0.0f, 45.0f, 0.0f, other.transform.rotation.w);
            //other.transform.localEulerAngles = new Vector3(0.0f, 45.0f, 0.0f);
            //other.transform.rotation.Set(0.0f, 45.0f, 0.0f, other.transform.rotation.w);
        }

    }

}
